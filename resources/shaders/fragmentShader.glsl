#version 330 core

in vec2 uvCoord;
in vec3 vertPosition;
in vec3 vertNormal;
in vec4 vertPosShadow;

uniform vec3 eyePosition;
uniform int objectType;

uniform sampler2D shadowMap;
uniform samplerCube skyBoxMap;

out vec4 outColor;

//light
vec3 lightPosition = vec3(0, -0.8, 0.2);//vec3(-0.3, 0, 0.9);
vec3 lightColor = vec3(1, 1, 1);

float specularStrenght = 0.5;


vec4 getLight(vec3 position, vec3 color, vec3 normal, int type, 
	float angle, vec3 direction);
float getShadow();



void main()
{
	if(objectType == 0)
	{
		outColor = vec4(0.5, 0.5, 0.5, 1);

		outColor *= vec4(0.2, 0.2, 0.2, 1) +
			(1. - getShadow()) * getLight(lightPosition, lightColor, vertNormal, 1, 0., vec3(0, 0, 0));
		//outColor = vec4(texture(shadowMap, uvCoord).r);
	}
	else if(objectType == 1)
	{
		float ratio = 0.45;
		vec3 I = normalize(vertPosition - eyePosition);
		vec3 R = refract(I, vertNormal, ratio);
		outColor = vec4(texture(skyBoxMap, R));
	}
	else if(objectType == 2)
	{
		vec3 I = normalize(vertPosition - eyePosition);
		vec3 R = reflect(I, vertNormal);
		outColor = vec4(texture(skyBoxMap, R));
	}
}
/////////////////////////////////////////////////////////////

vec4 getLight(vec3 position, vec3 color, vec3 normal, int type, 
	float angle, vec3 direction)
{

	float attenuation = 1.0;
	vec3 surfaceToLight;
	float distanceToLight;
	
	if(type == 1)
	{
		surfaceToLight = normalize(position);
	}
	if(type == 2 || type == 3)
	{
		surfaceToLight = normalize(position - vertPosition);
		distanceToLight = length(position - vertPosition);
		attenuation = 1.0 / (1.0 + 0.01 * pow(distanceToLight, 2));
	}
	if(type == 3)
	{
		direction = normalize(direction);
		vec3 rayDirection = -surfaceToLight;
		float lightToSurfaceAngle = degrees( acos( dot(rayDirection, direction)));
		if(lightToSurfaceAngle > angle)
			attenuation = 0;
	}

//diffuse

	float diffuseCoef = clamp( dot(normal, surfaceToLight), 0, 1);
	vec3 diffuse = color * diffuseCoef;
	
//specular
	vec3 surfaceToCamera = normalize(eyePosition - vertPosition);
	float specularCoef = 0;
	if(diffuseCoef > 0.0)
		specularCoef = pow( max( 0, dot(surfaceToCamera, reflect(-surfaceToLight, normal))), 5);
	vec3 specular = specularCoef * vec3(1, 1, 1) * specularStrenght;
	
	return vec4(attenuation*(diffuse+specular), 1);
}
////////////////////////////////////////////////////////////////////

float getShadow()
{
	vec3 worldPosition = vertPosShadow.xyz / vertPosShadow.w;
	worldPosition = worldPosition * 0.5 + 0.5;
	float shadowDepth = texture(shadowMap, worldPosition.xy).r;
	
	//float bias = max(0.0009 * (1.0 - dot(vertNormal, lightPosition)), 0.00005);
	
	return shadowDepth < worldPosition.z-0.05? 1.0 : 0;
}
