#version 330 core

layout(location = 0) in vec4 vertex;

out vec2 texCoords;

uniform mat4 projectionMatrix;

void main()
{
	gl_Position = projectionMatrix * vec4(vertex.xy, 0, 1);
	texCoords = vertex.zw;
}
