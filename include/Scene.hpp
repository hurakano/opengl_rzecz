#ifndef SCENE_HPP
#define SCENE_HPP

#include <Object.hpp>
#include <Tile.hpp>

#include <GL/glew.h>
#include <vector>


class Scene
{
public:

	Scene();
	void update(float time);
	void draw(GLuint shader);

	
	Object paddle;
	Object ball;
	std::vector<Tile> tiles;
	int score;
	bool gameover;
	
	glm::vec3 ballSpeed;

};

#endif//SCENE_HPP
