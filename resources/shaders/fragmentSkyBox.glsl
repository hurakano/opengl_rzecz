#version 330 core

in vec3 vertPosition;

out vec4 outColor;

uniform samplerCube skyBoxMap;

void main()
{
	outColor = vec4(texture(skyBoxMap, vertPosition));
}
