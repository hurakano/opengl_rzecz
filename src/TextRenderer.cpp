#include <TextRenderer.hpp>

#include <glem.hpp>

#include <iostream>
using namespace std;

TextRenderer::TextRenderer()
{
	FT_Library ft;
	FT_Face face;
	
	FT_Init_FreeType(&ft);
	FT_New_Face(ft, "resources/fonts/font.ttf", 0, &face);
	
	FT_Set_Pixel_Sizes(face, 0, 80);
	
	
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	
	glGenTextures(128, characters);

	for(unsigned char ch = 0; ch < 128; ch++)
	{
		FT_Load_Char(face, ch, FT_LOAD_RENDER);
		
		glBindTexture(GL_TEXTURE_2D, characters[ch]);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, face->glyph->bitmap.width, 
			face->glyph->bitmap.rows, 0, GL_RED, GL_UNSIGNED_BYTE, face->glyph->bitmap.buffer);
		width = face->glyph->bitmap.width;
		height = face->glyph->bitmap.rows;
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}
	
	glBindTexture(GL_TEXTURE_2D, 0);
	
	//glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
	
	FT_Done_Face(face);
	FT_Done_FreeType(ft);
	
	
	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vbo);
	
	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)*6*4, 0, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), 0);
	glEnableVertexAttribArray(0);
	
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	
	shader = loadShaders("resources/shaders/fontVertex.glsl", 
		"resources/shaders/fontFragment.glsl");
		
	projectionMatrix = glm::ortho(0., 700., 0., 700.);
}
//////////////////////////////////////////////////////////////

TextRenderer::~TextRenderer()
{
	glDeleteVertexArrays(1, &vao);
	glDeleteBuffers(1, &vbo);
	glDeleteProgram(shader);
	glDeleteTextures(128, characters);
}
/////////////////////////////////////////////////////////////

void TextRenderer::renderText(std::string text, float x, float y, float scale)
{
	glUseProgram(shader);
	glUniformMatrix4fv(glGetUniformLocation(shader, "projectionMatrix"), 
		1, GL_FALSE, value_ptr(projectionMatrix));
	
	glActiveTexture(GL_TEXTURE0);
	glBindVertexArray(vao);
	
	for(int i = 0; i < text.size(); i++)
	{
		float xpos = x * scale;
		float ypos = y * scale;
		float w = width * scale;
		float h = height * scale;
		
		GLfloat vertices[6][4] = 
		{
			{ xpos, ypos+h, 0, 0 },
			{ xpos, ypos, 0, 1},
			{ xpos+w, ypos+h, 1, 0},
			
			{ xpos+w, y, 1, 1},
			{ xpos, ypos, 0, 1},
			{ xpos+w, ypos+h, 1, 0}
		};
		
		glBindTexture(GL_TEXTURE_2D, characters[text[i]]);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		
		glDrawArrays(GL_TRIANGLES, 0, 6);
		
		x += width;
	}
	
	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glUseProgram(0);
}
