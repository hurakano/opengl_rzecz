#version 330 core

layout(location=0) in vec3 vertexPosition;
layout(location=1) in vec2 uvPosition;
layout(location=2) in vec3 normalVector;

out vec3 vertPosition;
out vec2 uvCoord;
out vec3 vertNormal;
out vec4 vertPosShadow;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

uniform mat4 shadowProjMatrix;
uniform mat4 shadowViewMatrix;


void main()
{
	mat4 matrix = projectionMatrix * viewMatrix * modelMatrix;
	gl_Position = matrix * vec4(vertexPosition, 1);


	vertPosition = gl_Position.xyz;
	vertPosShadow = shadowProjMatrix * shadowViewMatrix * modelMatrix * vec4(vertexPosition, 1);
	
	uvCoord = uvPosition;
	vertNormal = normalize(transpose(inverse(mat3(modelMatrix))) * normalVector);
}
