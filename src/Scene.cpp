#include <Scene.hpp>

#include <glm/gtc/matrix_transform.hpp>
#include <SFML/Window.hpp>

#define PAD_SPD 0.5
#define BALL_SPD 0.3

#include <iostream>
using namespace std;

using namespace sf;

Scene::Scene()
{
	paddle.loadFromFile("resources/models/box.obj");
	paddle.position = glm::vec3(0, -0.35, 0.2);
	paddle.modelMatrix = glm::scale(paddle.modelMatrix, glm::vec3(0.1, 0.01, 0.01));
	
	
	ball.loadFromFile("resources/models/ball.obj");
	ball.position = glm::vec3(0, 0, 0.2);
	ball.modelMatrix = glm::scale(ball.modelMatrix, glm::vec3(0.01, 0.01, 0.01));
	
	
	for(int i = 0; i < 10; i++)
		for(int j = 0; j < 10; j++)
		{
			Tile tile;
			tile.loadFromFile("resources/models/box.obj");
			tile.modelMatrix = glm::scale(tile.modelMatrix, glm::vec3(0.03, 0.01, 0.01));
			tile.position = glm::vec3(-0.35+(0.08*i), 0.4-(0.04*j), 0.2);
			
			if(i > 0 && i < 9 && (i + (j%2)) % 2)tile.objectType = 1;
			else tile.objectType = 0;
			
			tiles.push_back(tile);
		}
		
	score = 0;
	gameover = 0;
	ballSpeed = glm::vec3(0, -BALL_SPD, 0);
}
///////////////////////////////////////////

void Scene::update(float time)
{
//paddleMovement
	glm::vec3 paddelMove(0);
	
	if(Keyboard::isKeyPressed(Keyboard::Left))
		paddelMove.x -= PAD_SPD * time;
	if(Keyboard::isKeyPressed(Keyboard::Right))
		paddelMove.x += PAD_SPD * time;
	if(Keyboard::isKeyPressed(Keyboard::Up))
		paddelMove.y += PAD_SPD * time;
	if(Keyboard::isKeyPressed(Keyboard::Down))
		paddelMove.y -= PAD_SPD * time;
	
	if(paddle.position.x + paddelMove.x < -0.3 || paddle.position.x + paddelMove.x > 0.3)
		paddelMove = glm::vec3(0);
		
	paddle.position += paddelMove;


//ball with paddle
	glm::vec3 ballPosition = ball.position + ballSpeed * time;
		
	if(ballPosition.y < paddle.position.y + 0.02 && ballPosition.y > paddle.position.y - 0.02 &&
		ballPosition.x > paddle.position.x - 0.1 &&
		ballPosition.x <= paddle.position.x)
	{
		ballSpeed = glm::vec3(
			(paddle.position.x - ballPosition.x)/0.1 * -BALL_SPD, 
			BALL_SPD, 0);
	}	
	else if(ballPosition.y < paddle.position.y + 0.02 && ballPosition.y > paddle.position.y - 0.02 &&
		ballPosition.x < paddle.position.x + 0.1 &&
		ballPosition.x > paddle.position.x)
	{
		ballSpeed = glm::vec3(
			(ballPosition.x - paddle.position.x)/0.1 * BALL_SPD, 
			BALL_SPD, 0);
	}
	ballPosition = ball.position + ballSpeed * time;
		
	if(ballPosition.y > 0.4)
		ballSpeed.y = -ballSpeed.y;
		
	if(ballPosition.x < -0.4 || ballPosition.x > 0.4)
		ballSpeed.x = -ballSpeed.x;
		
	ballPosition = ball.position + ballSpeed * time;

	if(ballPosition.y < -0.6)
		gameover = 1;
//ball with tiles
	for(auto it = tiles.begin(); it < tiles.end();)
	{
		if(ballPosition.x > it->position.x - 0.05 && ballPosition.x < it->position.x + 0.05 &&
			ballPosition.y < it->position.y + 0.01 && ballPosition.y > it->position.y - 0.01)
		{
			ballSpeed.y = -ballSpeed.y;
			if(it->objectType == 1)
			{
				it->objectType = 0;
				it++;
				continue;
			}
			tiles.erase(it);
			score += 100;
		}
		else
		{
			it++;
		}
	}
	
	ball.position += ballSpeed * time;
	
	if(tiles.size() == 0)
		gameover = 2;
	
	//cout<<paddle.position.x<<" "<<ball.position.x<<"\r"<<flush;
}
////////////////////////////////////////////////////////////////////

void Scene::draw(GLuint shader)
{
	glUniform1i(glGetUniformLocation(shader, "objectType"), 2);
	ball.draw(shader);
	
	glUniform1i(glGetUniformLocation(shader, "objectType"), 0);
	paddle.draw(shader);
	
	for(auto x: tiles)
	{
		if(x.objectType == 1)
			glUniform1i(glGetUniformLocation(shader, "objectType"), 1);
		else
			glUniform1i(glGetUniformLocation(shader, "objectType"), 0);
			
		x.draw(shader);
	}
}
