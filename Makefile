.PHONY: clean

FLAGS=--std=c++11 -Iinclude/ -I. -L. -I/usr/include/freetype2
SRC=$(wildcard src/*.cpp)
HPP=$(wildcard include/*.hpp)
OBJ=$(patsubst src/%.cpp,obj/%.o,$(SRC))
LIB=-lsfml-graphics -lsfml-window -lsfml-system -lglem -lGLEW -lGL -lfreetype

main:main.cpp $(OBJ) $(SRC) $(HPP)
	g++ -o $@ main.cpp $(OBJ) $(FLAGS) $(LIB)
	
obj/%.o:src/%.cpp include/%.hpp
	g++ -o $@ $< $(FLAGS) -c
	
clean:
	rm obj/* main
