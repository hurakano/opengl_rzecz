#version 330 core

in vec2 texCoords;

out vec4 fragColor;

uniform sampler2D tex;

vec3 color = vec3(0.8, 0.3, 0.7);

void main()
{
	vec4 sampl = vec4(1, 1, 1, texture(tex, texCoords).r);
	fragColor = vec4(color, 1) * sampl;
}
