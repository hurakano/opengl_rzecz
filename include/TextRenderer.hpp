#ifndef TEXT_RENDERER_HPP
#define TEXT_RENDERER_HPP

#include <freetype2/ft2build.h>
#include FT_FREETYPE_H

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <string>

class TextRenderer
{
public:

	TextRenderer();
	~TextRenderer();
	
	void renderText(std::string text, float x, float y, float scale);
	
private:

	GLuint characters[128];
	GLuint shader;
	GLuint vao;
	GLuint vbo;
	float width;
	float height;
	glm::mat4 projectionMatrix;
};

#endif//TEXT_RENDERER_HPP
