#version 330 core

layout(location=0) in vec3 vertexPosition;
layout(location=1) in vec2 uvPosition;
layout(location=2) in vec3 normalVector;

out vec3 vertPosition;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;


void main()
{
	mat4 matrix = projectionMatrix * viewMatrix * modelMatrix;
	gl_Position = matrix * vec4(vertexPosition, 1);

	vertPosition = vertexPosition;
}
