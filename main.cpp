#include <GL/glew.h>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include <vector>
#include <cmath>

#include "glem.hpp"

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#define PI 3.14159265359


using namespace sf;
using namespace std;
using namespace glm;

#include <Object.hpp>
#include <Scene.hpp>
#include <TextRenderer.hpp>

#define WINDOW_WIDTH 700
#define WINDOW_HEIGHT 700
#define SHADOW_WIDTH 1000
#define SHADOW_HEIGHT 1000

GLuint loadCubeMap(vector<string> names);
void makeShadowMap(GLuint *frameBuffer, GLuint *shadowMap);
void prepareShadowRender(GLuint *frameBuffer, GLuint *shadowMap);
void initializeObjects(vector<Object> &objects);

int main()
{
//make bindow
	ContextSettings settings;
	settings.depthBits = 24;
	settings.majorVersion = 3;
	settings.minorVersion = 3;
	settings.attributeFlags = ContextSettings::Core;
	RenderWindow window(VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), "okno", Style::Default, settings);

	glewExperimental=GL_TRUE;
	glewInit();

	float x{0}, y{0}, s{0}, u{0}, b{0};
	float scal = 1;
	bool menuActive = 1;
	
//init config
	
	GLuint shader = loadShaders("resources/shaders/vertexShader.glsl", 
		"resources/shaders/fragmentShader.glsl");
	GLuint skyBoxShader = loadShaders("resources/shaders/vertexSkyBox.glsl", 
		"resources/shaders/fragmentSkyBox.glsl");
	GLuint shadowShader = loadShaders("resources/shaders/vertexShader.glsl", 
		"resources/shaders/fragmentShadow.glsl");
	
	glUseProgram(shader);
	glUniform1i(glGetUniformLocation(shader, "shadowMap"), 0);
	glUniform1i(glGetUniformLocation(shader, "skyBoxMap"), 1);
	glUseProgram(0);
	
	GLuint shadowFramebuffer, depthShadowMap;
	prepareShadowRender(&shadowFramebuffer, &depthShadowMap);
//load objects

	GLuint skyBoxMap = loadCubeMap(vector<string>{"resources/textures/skyBox/right.png", 
		"resources/textures/skyBox/left.png", "resources/textures/skyBox/top.png", 
		"resources/textures/skyBox/bottom.png", "resources/textures/skyBox/front.png",
		"resources/textures/skyBox/back.png"});
	Object skyBox;
	skyBox.loadFromFile("resources/models/box.obj");
	//skyBox.modelMatrix = translate(skyBox.modelMatrix, vec3(0, 0, 0.7));
	//skyBox.modelMatrix = scale(skyBox.modelMatrix, vec3(1, 1, 1));
//objects
	Scene scene;
	TextRenderer textRenderer;

	
	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//glEnable(GL_CULL_FACE);
	//glCullFace(GL_BACK);
	
	//mat4 projMatrix = ortho(-1, 1, -1, 1, 1, 3);
	mat4 projMatrix = perspective(45.f, 1.f, 0.1f, 100.f);
	
	mat4 viewMatrix =  lookAt(vec3(0, -0.7, 0.6), vec3(0, 0, 0), vec3(0, 1, 0));
	s=0, u=-0.7, b=0.6;
	//mat4 viewMatrix =  lookAt(vec3(0, 0, 1), vec3(0, 0, 0), vec3(0, 1, 0));
		
	mat4 shadowViewMatrix = lookAt(vec3(0, -0.8, 0.1), vec3(0, 0, 0), vec3(0, 1, 0));
	mat4 shadowProjectionMatrix = ortho(-1., 1., -1., 1., 0.8, 1.6);
	
	
	Clock frameTimer;
	
//start of main loop	
	while(window.isOpen())
	{
		Event event;
		
		if(window.pollEvent(event))
		{		
			if(event.type == Event::Closed || event.type == Event::KeyPressed && event.key.code == Keyboard::Escape)
			{
				window.close();
				break;
			}
			if(event.type == Event::KeyPressed && event.key.code == Keyboard::W)
				u += 0.1;
			if(event.type == Event::KeyPressed && event.key.code == Keyboard::S)
				u -= 0.1;
			if(event.type == Event::KeyPressed && event.key.code == Keyboard::A)
				s -= 0.1;
			if(event.type == Event::KeyPressed && event.key.code == Keyboard::D)
				s += 0.1;
			if(event.type == Event::KeyPressed && event.key.code == Keyboard::R)
				b += 0.3;
			if(event.type == Event::KeyPressed && event.key.code == Keyboard::F)
				b -= 0.3;
			if(event.type == Event::KeyPressed && event.key.code == Keyboard::E)
				s -= 0.3;
			if(event.type == Event::KeyPressed && event.key.code == Keyboard::T)
				s += 0.3;
			if(event.type == Event::KeyPressed && event.key.code == Keyboard::Q)
				b -= 0.3;
			if(event.type == Event::KeyPressed && event.key.code == Keyboard::Z)
				b += 0.3;
		}
     	if(FloatRect(Vector2f(280, 320), Vector2f(100, 20)).contains(
     		Vector2f(Mouse::getPosition(window))) && Mouse::isButtonPressed(Mouse::Left))
     	{
     		menuActive = 0;
     		frameTimer.restart();
     	}
		if(!menuActive)
			scene.update(frameTimer.restart().asSeconds());
     	
     	viewMatrix = lookAt(vec3(s, u, b), vec3(0, 0, 0), vec3(0, 1, 0));
//render
	
	//make shadow map
		glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
		glBindFramebuffer(GL_FRAMEBUFFER, shadowFramebuffer);
		glClear(GL_DEPTH_BUFFER_BIT);
		
		glUseProgram(shadowShader);
		glUniformMatrix4fv(glGetUniformLocation(shadowShader, "projectionMatrix"), 
        	1, GL_FALSE, value_ptr(shadowProjectionMatrix));
        glUniformMatrix4fv(glGetUniformLocation(shadowShader, "viewMatrix"), 
        	1, GL_FALSE, value_ptr(shadowViewMatrix));
        
       	scene.paddle.draw(shadowShader);
       	scene.ball.draw(shadowShader);
       	for(Object x: scene.tiles)
       		x.draw(shadowShader);
			
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
	
	//end of shadow map
		
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
		
	//skybox
		glDepthMask(GL_FALSE);
		glUseProgram(skyBoxShader);
		
		glUniform3f(glGetUniformLocation(shader, "eyePosition"), 0, 0, 1);
        glUniformMatrix4fv(glGetUniformLocation(shader, "projectionMatrix"), 
        	1, GL_FALSE, value_ptr(projMatrix));
        glUniformMatrix4fv(glGetUniformLocation(shader, "viewMatrix"), 
        	1, GL_FALSE, value_ptr(viewMatrix));
		
		glBindTexture(GL_TEXTURE_CUBE_MAP, skyBoxMap);
		
		skyBox.draw(skyBoxShader);
		
		glUseProgram(0);
		glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
		glDepthMask(GL_TRUE);
	//end of skybox
	
		
		glUseProgram(shader);
		
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, depthShadowMap);
		glActiveTexture(GL_TEXTURE0+1);
		glBindTexture(GL_TEXTURE_CUBE_MAP, skyBoxMap);
		
		glUniform3f(glGetUniformLocation(shader, "eyePosition"), 0, 0, 1);
        glUniformMatrix4fv(glGetUniformLocation(shader, "projectionMatrix"), 
        	1, GL_FALSE, value_ptr(projMatrix));
        glUniformMatrix4fv(glGetUniformLocation(shader, "viewMatrix"), 
        	1, GL_FALSE, value_ptr(viewMatrix));
        glUniformMatrix4fv(glGetUniformLocation(shader, "shadowProjMatrix"), 
        	1, GL_FALSE, value_ptr(shadowProjectionMatrix));
        glUniformMatrix4fv(glGetUniformLocation(shader, "shadowViewMatrix"), 
        	1, GL_FALSE, value_ptr(shadowViewMatrix));
		
		glUniform1i(glGetUniformLocation(shader, "objectType"), 0);
		
		scene.draw(shader);
		
		
		glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, 0);
		
		glUseProgram(0);
		
		if(menuActive && !scene.gameover)
			textRenderer.renderText("Start", 280, 350, 1);
		else if(scene.gameover == 1)
		{
			menuActive = 1;
			textRenderer.renderText("GAME OVER", 250, 350, 1);
		}
		else if(scene.gameover == 2)
		{
			menuActive = 1;
			textRenderer.renderText("YOU WON", 250, 350, 1);
		}
		else
			textRenderer.renderText(to_string(scene.score), 600, 640, 1);
		
		window.display();
	}
	
	return 0;
}
//////////////////////////////////////////////////////////////////

GLuint loadCubeMap(vector<string> names)
{
	GLuint cubeMapId;
	glGenTextures(1, &cubeMapId);
	glBindTexture(GL_TEXTURE_CUBE_MAP, cubeMapId);
	
	int width, height, n;
	for(int i = 0; i < 6; i++)
	{
		unsigned char *data = stbi_load(names[i].c_str(), &width, &height, &n, 0); 
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, 
			width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
		stbi_image_free(data);
	}
	
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	
	return cubeMapId;
}
/////////////////////////////////////////////////////

void prepareShadowRender(GLuint *frameBuffer, GLuint *shadowMap)
{
	glGenFramebuffers(1, frameBuffer);
	
	glGenTextures(1, shadowMap);
	glBindTexture(GL_TEXTURE_2D, *shadowMap);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH, SHADOW_HEIGHT, 
		0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	
	
	glBindFramebuffer(GL_FRAMEBUFFER, *frameBuffer);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, *shadowMap, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}
///////////////////////////////////////////////////////////

void initializeObjects(vector<Object> &objects)
{
	Object object;
	object.loadFromFile("resources/models/box.obj");
	object.modelMatrix = translate(object.modelMatrix, vec3(0, 0, -0.4));
	object.modelMatrix = scale(object.modelMatrix, vec3(0.2, 0.2, 0.2));
	
	objects.push_back(object);
	
	object.loadFromFile("resources/models/box.obj");
	object.modelMatrix = translate(object.modelMatrix, vec3(0, -0.4, -0.4));
	object.modelMatrix = scale(object.modelMatrix, vec3(0.1, 0.1, 0.1));
	
	objects.push_back(object);
	
	object.loadFromFile("resources/models/plane.obj");
	object.modelMatrix = translate(object.modelMatrix, vec3(0, -0.4, 2));
	object.modelMatrix = rotate(object.modelMatrix, 1.5f, vec3(1, 0, 0));
	object.modelMatrix = scale(object.modelMatrix, vec3(1, 1, 1));
	
	objects.push_back(object);
}
/////////////////////////////////////////////////////
