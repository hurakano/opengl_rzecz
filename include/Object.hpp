#ifndef OBJECT_HPP
#define OBJECT_HPP

#include <glm/glm.hpp>
#include <string>
#include <GL/glew.h>


class Object
{
public:
	
	
	void loadFromFile(std::string);
	void loadTexture(std::string filename);
	void draw(GLuint shader);
	
	
	GLuint vertexArray;
	GLuint vertexBuffer;
	GLuint indexBuffer;
	GLuint texture;
	int indexCount;
	
	GLuint shader;
	
	glm::vec3 position;
	glm::mat4 modelMatrix;
	
};

#endif//OBJECT_HPP
