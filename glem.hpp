#ifndef GLEM_HPP
#define GLEM_HPP

#include<vector>
#include<string>
#include<glm/glm.hpp>
#include<iostream>

//loads one 'shaderType' shader from 'fileName'
GLuint loadShader(std::string shaderFileName, GLuint shaderType, std::ostream &logStream = std::cout);

//loads program
GLuint loadShaders(std::string vertexShaderName, std::string fragmentShaderName, 
	std::string geometryShaderName = "", std::ostream &logStream = std::cout);

//obj loader

struct ObjData
{
	std::vector<float> data;
	
	std::vector<unsigned int> faces;
	
	std::string textureFileName;
	std::string normalMapFileName;
};

int loadObjFile(std::string fileName, ObjData *obj);
glm::uvec3 loadVertex(std::string &face);
std::vector<glm::uvec3> loadFace(std::string &face);
void loadMtl(std::ifstream &file, ObjData &obj);

#endif//GLEM_HPP
