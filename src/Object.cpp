#include <Object.hpp>

#include <glem.hpp>

#include <stb_image.h>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>

/////////////////////////////////////////////////////////////////

void Object::loadFromFile(std::string filename)
{
	position = glm::vec3(0, 0, 0);
	modelMatrix = glm::mat4(1);
	shader = 0;
	texture = 0;
	
	ObjData data;
	
	loadObjFile(filename, &data);
	
	glGenVertexArrays(1, &vertexArray);
	glGenBuffers(1, &vertexBuffer);
	glGenBuffers(1, &indexBuffer);
	
	glBindVertexArray(vertexArray);
	
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, data.data.size()*sizeof(float), data.data.data(), GL_STATIC_DRAW);
	
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8*sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 8*sizeof(float), (void*)(3*sizeof(float)));
	glEnableVertexAttribArray(1);
	
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 8*sizeof(float), (void*)(5*sizeof(float)));
	glEnableVertexAttribArray(2);
	
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, data.faces.size()*sizeof(float), data.faces.data(), GL_STATIC_DRAW);
	indexCount = data.faces.size();
	
	glBindVertexArray(0);
	
	if(data.textureFileName != "")
		loadTexture(data.textureFileName);
}
///////////////////////////////////////////////////////

void Object::loadTexture(std::string filename)
{
	glGenTextures(1, &texture);
	
	int width, height, n;
	unsigned char *imageData = stbi_load(filename.c_str(), &width, &height, &n, 0);
	
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, imageData);
	glBindTexture(GL_TEXTURE_2D, 0);
	
	stbi_image_free(imageData);
}

void Object::draw(GLuint shader)
{
	glm::mat4 modelMat(1); //modelMatrix;
	modelMat = glm::translate(modelMat, position);
	modelMat = modelMat * modelMatrix;
	
	glUniformMatrix4fv(glGetUniformLocation(shader, "modelMatrix"), 1, GL_FALSE, value_ptr(modelMat));
	
	if(texture)
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture);
	}
	glBindVertexArray(vertexArray);
	
	glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, (void *)0);
	
	glBindVertexArray(0);
	if(texture)
		glBindTexture(GL_TEXTURE_2D, 0);
	
}
